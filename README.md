## Project ReactJS TODO App


## Install

$ git clone https://gitlab.com/amXCVIexceed/todo-frontend.git
$ cd todo-frontend
$ npm install


## Configure app

Copy .example.env to .env then edit it with the url where you have setup backend api.
    

## Start & watch

$ npm start

## Simple build for production

$ npm run build


## Update sources

Some packages usages might change so you should run npm prune & npm install often. A common way to update is by doing

$ git pull
$ npm prune
$ npm install

To run those 3 commands you can just do

$ npm run pull


## Languages & tools

# HTML
    Jade for some templating.
# JavaScript
    React is used for UI.
# CSS
    cssnext is used to write futureproof CSS for CSS vendor prefix under the hood).

# Static server with Livereload
The app embed for development a static connect server with livereload plugged. So each time you start the app, you get automatic refresh in the browser whenever you update a file.