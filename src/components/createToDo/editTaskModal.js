import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class Container extends Component {

    render() {
        return (
            <div clasName="modal_window">
                <Button color="danger">Test</Button>
                <Modal isOpen='true' className='Modal'>
                    <ModalHeader className="modal_header"><h1>Edit task</h1></ModalHeader>
                    <ModalBody>
                    <div className="input-form">
                        <form className="form">
                            <input type="text" className="form-field" placeholder="Enter the task" name="contenttask" onChange={this.props.handleChange} />
                        </form>
                    </div>
                    </ModalBody>
                    <ModalFooter>
                        <button className="sign_btn sign_btn_modal" color="primary" onClick={this.props.handleChangeEditTask}>Save</button>{' '}
                        <button className="sign_btn sign_btn_modal" color="secondary" onClick={this.props.handleCloseEditTask}>Cancel</button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
}

export default Container;