import React, { Component } from 'react';

class TodoAddTask extends Component {

    render() {
        return (
            <div className="input-form">
                <input type="text" id="inputTaskText" className="form-field" placeholder="Enter the task..." name="contenttask" onKeyDown={this.props.handleChange} onChange={this.props.handleChange} value={this.props.state.contenttask}/>
            </div>
        )
    }
}

export default TodoAddTask;