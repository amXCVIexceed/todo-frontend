import React, { Component } from 'react';
import axios from 'axios';
import EditTaskModal from "./editTaskModal";
import TodoAddTask from "./addTask";
import ToastErr from "../toasts/toastErr";

class TodoHeader extends Component {

    constructor() {
        super();
        this.state = {
            id: '',
            userid: 0,
            contenttask: '',
            state: false,
            username: '',
            tasks: [],
            isModalOpen: false,
            isErrorsText: false,
            quantityTasks: null,
            quantityDoneTasks: null,
        };
    }

    componentDidMount = async () => {
        const token = localStorage.getItem('token');
        if (token) {
            axios.defaults.headers.common['Authorization'] = token;
        } else {
            axios.defaults.headers.common['Authorization'] = 'unded';
        }

        axios.get(`${process.env.REACT_APP_API_URL}/task/getall`, {
            params: { token: localStorage.getItem('token') }
        }).then(res => {
            this.setState({ tasks: res.data });
            this.getTasksInformation();
            this.props.setTaskInfo(this.state.quantityTasks, this.state.quantityDoneTasks);
        }).catch(err => {
            this.setState({ isErrorsText: err.response.data });
            setTimeout(this.handleCloseErrors, 6000);
        })
    }

    getTasksInformation = () => {
        const quantity = this.state.tasks.length;
        const quantityDone = this.state.tasks.filter(function (res) {
            return res.state === true;
        }).length;
        return this.setState({ quantityTasks: quantity, quantityDoneTasks: quantityDone })
    }

    updateFromState = (id) => {
        const index = this.state.tasks.findIndex(task => task.id === id);
        const arrTasks = this.state.tasks;
        arrTasks[index].state = !arrTasks[index].state;
        arrTasks[index].contenttask = this.state.contenttask;
        this.setState({ tasks: arrTasks });
        this.getTasksInformation();
        this.props.setTaskInfo(this.state.quantityTasks, this.state.quantityDoneTasks);
        this.clearContenttaskFromState();
        this.setState({ state: false });
    }

    addFromState = (id) => {
        const addTask = {
            id: id,
            userid: this.state.userid,
            contenttask: this.state.contenttask,
            username: this.state.username,
            state: this.state.state,
        };
        const arrTasks = this.state.tasks;
        arrTasks.push(addTask);
        this.setState({ tasks: arrTasks });
        this.getTasksInformation();
        this.props.setTaskInfo(this.state.quantityTasks, this.state.quantityDoneTasks);
        this.clearContenttaskFromState();
    }

    clearContenttaskFromState = () => {
        this.setState({ contenttask: '' });
    }

    deleteFromState = async (id) => {
        const arrTasks = this.state.tasks;
        arrTasks.splice(arrTasks.findIndex(task => task.id === id), 1)
        this.setState({ tasks: arrTasks });
        await this.getTasksInformation();
        this.props.setTaskInfo(this.state.quantityTasks, this.state.quantityDoneTasks);
    }

    add = () => {
        const config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            }
        }

        const data = new URLSearchParams();
        data.append('id', this.state.id);
        data.append('userid', this.state.userid);
        data.append('contenttask', this.state.contenttask);
        data.append('state', this.state.state);
        data.append('token', localStorage.getItem('token'));

        axios.post(`${process.env.REACT_APP_API_URL}/task/create`, data, config)
            .then(res => {
                this.addFromState(res.data.id);
            }).catch(err => {
                this.setState({ isErrorsText: err.response.data });
                setTimeout(this.handleCloseErrors, 6000);
            })
    }

    delete = (event) => {
        axios.delete(`${process.env.REACT_APP_API_URL}/task/delete`, {
            data: { id: event }
        }).catch(err => {
            this.setState({ isErrorsText: err.response.data });
            setTimeout(this.handleCloseErrors, 6000);
        })
        this.deleteFromState(event);
    }

    update = (id, state) => {
        const config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            }
        }

        const data = new URLSearchParams();
        data.append('id', id);
        data.append('contenttask', this.state.contenttask);
        data.append('state', state);
        axios.put(`${process.env.REACT_APP_API_URL}/task/update`, data, config)
            .then(res => {
                this.updateFromState(id);
            }).catch(err => {
                this.setState({ isErrorsText: err.response.data });
                setTimeout(this.handleCloseErrors, 6000);
            })

    }

    handleCheckboxChange = (id, status) => {
        this.state.tasks.forEach(data => {
            if (data.id === id) {
                if (data.state) {
                    this.setState({ state: false });
                } else {
                    this.setState({ state: true });
                }
                this.setState({ contenttask: data.contenttask });
                this.setState({ userid: data.userid });
                this.update(id, status);
                this.setState({ id: id });

                return;
            }
        })
    }

    isNoEmptyTask = () => {
        if (this.state.contenttask === '') {
            this.setState({ isErrorsText: 'empty task!' });
            setTimeout(this.handleCloseErrors, 6000);
            return false;
        } else {
            return true;
        }
    }

    handleChange = (event) => {
        if (event.key !== 'Enter') {
            this.setState({ [event.target.name]: event.target.value });
        } else {
            if (this.isNoEmptyTask()) {
                this.add();
            }
        }
    }

    handleClose = (event) => {
        this.delete(parseInt(event.target.id.match(/\d+/), 10));
    }

    handleOpenEditTask = (id) => {
        this.setState({ isModalOpen: true });
        this.setState({ id: id });
    }

    handleCloseEditTask = () => {
        this.setState({ isModalOpen: false });
    }

    handleChangeEditTask = () => {
        if (this.isNoEmptyTask()) {
            this.state.tasks.forEach(data => {
                if (data.id === this.state.id) {
                    this.setState({ userid: data.userid });
                    this.update(this.state.id, this.state.state);
                    this.handleCloseEditTask();
                    return;
                }
            })
        }
    }

    handleSignOut = () => {
        localStorage.removeItem('token');
        this.props.updateIsLogged();
        this.props.updateIsShowMyProfile();
    }

    handleCloseErrors = () => {
        this.setState({ isErrorsText: false });
    }

    render() {
        return (
            <div className="col-md-12 parent-list">

                <div className="col-md-12 todo-list" >
                    <TodoAddTask
                        add={this.add}
                        handleChange={this.handleChange}
                        state={this.state}
                    />

                    <div className="row list-tasks">
                        {this.state.tasks.map((res, i) =>
                            <div className="row list-item" id={`task${res.id}`} key={i}>
                                <div className="col-md-10 task--label">
                                    <label className="the-task">
                                        <input type="checkbox" className="checkbox-input" name="chetyre" defaultChecked={res.state} onClick={() => this.handleCheckboxChange(res.id, !res.state)} />
                                        <span className="task-label" id={`checkbox${res.id}`}>{res.contenttask}</span>
                                    </label>
                                </div>
                                <div className="col-md-2">
                                    <div className="row">
                                        <div className="col-md-2">
                                            < button type="button" id={'edit' + res.id} className="close" onClick={() => this.handleOpenEditTask(res.id)}>&#9998;</button>
                                        </div>
                                        <div className="col-md-6">
                                            < button type="button" id={'close' + res.id} className="close" onClick={this.handleClose} >&times;</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                </div>

                {this.state.isModalOpen ?
                    <EditTaskModal
                        handleChange={this.handleChange}
                        handleCloseEditTask={this.handleCloseEditTask}
                        handleChangeEditTask={this.handleChangeEditTask}
                    /> : <div></div>
                }

                {this.state.isErrorsText &&
                    <ToastErr
                        message={this.state.isErrorsText}
                    />
                }

                <div className="row">
                    < button type="button" className="sign_btn sign_btn_out" onClick={this.handleSignOut} >sign out</button>
                </div>
            </div>
        )
    }
}


export default TodoHeader;