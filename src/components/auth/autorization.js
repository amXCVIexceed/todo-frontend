import React, { Component } from 'react';

class TodoRegistration extends Component {

    render() {
        return (
            <div className="col-md-12">
                <div className="container_auth">
                    <h1>Sign in</h1>
                    <form action="#" onSubmit={this.props.handleSubmit} className="sign-in-form">
                        <input
                            type="text"
                            placeholder="email"
                            className="field"
                            onChange={this.props.handleChangeEmail}
                        />
                        <input
                            type="password"
                            placeholder="password"
                            className="field"
                            onChange={this.props.handleChangePasswod}
                        />
                        <button
                            type="button"
                            value="Sign in"
                            onClick={this.props.handleSignIn}
                            className="loggedbtn">
                            Sign in
                                </button>
                    </form>
                    <div className="pass-link">
                        <a href="#" onClick={this.props.updateIsRegister}>Register</a>
                    </div>
                </div>
            </div>
        )
    }
}

export default TodoRegistration;