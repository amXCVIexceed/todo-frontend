import React, { Component } from 'react';
import axios from 'axios';
import TodoRegistration from "./registration";
import TodoAutorization from "./autorization";
import ToastErr from "../toasts/toastErr";

class TodoAuth extends Component {

    constructor() {
        super();
        this.state = {
            userid: 0,
            email: '',
            username: '',
            lastname: '',
            password: '',
            isRegister: true,
            isValidEmail: false,
            isValidPassword: false,
            isErrorsText: false,
        };
    }

    updateIsRegister = () => {
        this.setState({ isRegister: !this.state.isRegister });
    }

    handleSignIn = () => {
        if (!this.state.isValidEmail) {
            this.setState({ isErrorsText: 'Invalid email!' });
        }
        else {
            if (!this.state.isValidPassword) {
                this.setState({ isErrorsText: 'Invalid password!' });
            } else {
                this.login();
            }
        }
        setTimeout(this.handleCloseErrors, 6000);
        this.setState({ isValidEmail: false });
        this.setState({ isValidPassword: false });
    }

    handleSignUp = () => {
        this.newUser();
        this.updateIsRegister();
        this.handleSignIn();
    }


    login = () => {
        const config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            }
        }
        const data = new URLSearchParams();
        data.append('email', this.state.email);
        data.append('password', this.state.password);

        axios.post(`${process.env.REACT_APP_API_URL}/user/login`, data, config)
            .then(res => {
                localStorage.setItem('token', res.data.token);
                this.props.updateIsLogged();
            }).catch(err => {
                this.setState({ isErrorsText: err.response.data });
                setTimeout(this.handleCloseErrors, 6000);
            })

    }

    newUser = () => {
        const config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            }
        }

        const data = new URLSearchParams();
        data.append('email', this.state.email);
        data.append('firstname', this.state.username);
        data.append('lastname', this.state.lastname);
        data.append('password', this.state.password);

        axios.post(`${process.env.REACT_APP_API_URL}/user/create`, data, config)
            .catch(err => {
                this.setState({ isErrorsText: err.response.data });
                setTimeout(this.handleCloseErrors, 6000);
            })
    }

    validateEmail = (value) => {
        const emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        const isValidEmail = emailValid ? true : false;
        this.setState({ isValidEmail: isValidEmail });
    }

    validatePassword = (value) => {
        const passwordValid = value.match(/^([a-zA-Z0-9+&@#/%?=~_|!:,.;]{1,})$/i);
        const isValidPassword = passwordValid ? true : false;
        this.setState({ isValidPassword: isValidPassword });
    }

    handleChangeEmail = (event) => {
        this.validateEmail(event.target.value);
        this.setState({ email: event.target.value });
    }

    handleChangeUsername = (event) => {
        this.setState({ username: event.target.value });
    }

    handleChangeLastname = (event) => {
        this.setState({ lastname: event.target.value })
    }

    handleChangePasswod = (event) => {
        this.validatePassword(event.target.value);
        this.setState({ password: event.target.value });
    }

    handleCloseErrors = () => {
        this.setState({ isErrorsText: false });
    }

    render() {
        return (
            <div>
                <div className="row justify-content-md-center">
                    <div className="row">
                    </div>
                    <div className="col-md-12">
                        {this.state.isRegister ?
                            <TodoAutorization
                                handleChangeEmail={this.handleChangeEmail}
                                handleChangePasswod={this.handleChangePasswod}
                                handleSignIn={this.handleSignIn}
                                updateIsRegister={this.updateIsRegister}
                            />
                            : <TodoRegistration
                                handleChangeEmail={this.handleChangeEmail}
                                handleChangeLastname={this.handleChangeLastname}
                                handleChangeFirstname={this.handleChangeUsername}
                                handleChangePasswod={this.handleChangePasswod}
                                handleSignUp={this.handleSignUp}
                                updateIsRegister={this.updateIsRegister}
                            />}
                    </div>
                </div>
                {this.state.isErrorsText &&
                    <ToastErr
                        message={this.state.isErrorsText}
                    />
                }
            </div>
        )
    }
}

export default TodoAuth;