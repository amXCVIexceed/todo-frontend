import React, { Component } from 'react';
import UserAvatar from './avatars'
import axios from 'axios';

class UserProfile extends Component {

    constructor(props) {
        super(props)
        this.state = {
            email: this.props.user.email,
            avatar: this.props.user.avatar,
        }
    }


    changePasswordUser = async () => {
        const config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            }
        }
        this.props.setInfoText('Password reset email sent!');
        const data = new URLSearchParams();
        data.append('token', localStorage.getItem('token'));

        axios.get(`${process.env.REACT_APP_API_URL}/user/recovery`, {
            params: { token: localStorage.getItem('token') }
        }, config)
    }

    render() {
        return (
            <div className="container_user_info">
                <h2>Hello, {this.props.user.firstname}</h2>
                <UserAvatar
                    setAvatar={this.setAvatar}
                    email={this.state.email}
                    avatar={this.state.avatar}
                />
                <div className="row row_user_info">
                    <div className="col-md-5">
                        <span className="user_signature">
                            email:
                        </span>
                    </div>
                    <div className="col-md-7">
                        <span className="user_value">
                            {this.props.user.email}
                        </span>
                    </div>
                </div>
                <div className="row row_user_info">
                    <div className="col-md-5">
                        <span className="user_signature">
                            firstname:
                        </span>
                    </div>
                    <div className="col-md-7">
                        <span className="user_value">
                            {this.props.user.firstname}
                        </span>
                    </div>
                </div>
                <div className="row row_user_info">
                    <div className="col-md-5">
                        <span className="user_signature">
                            lastname:
                        </span>
                    </div>
                    <div className="col-md-7">
                        <span className="user_value">
                            {this.props.user.lastname}
                        </span>
                    </div>
                </div>
                <h6>statistics:</h6>
                <div className="row row_user_info">
                    <div className="col-md-5">
                        <span className="user_signature">
                            total tasks:
                        </span>
                    </div>
                    <div className="col-md-7">
                        <span className="user_value">
                            {this.props.taskInfo.quantity}
                        </span>
                    </div>
                </div>
                <div className="row row_user_info">
                    <div className="col-md-5">
                        <span className="user_signature">
                            done:
                        </span>
                    </div>
                    <div className="col-md-7">
                        <span className="user_value">
                            {this.props.taskInfo.quantityDone}
                        </span>
                    </div>
                </div>

                <div className="pass-link">
                    <a href="#" onClick={this.changePasswordUser}>Change password</a>
                </div>
                <div className="pass-link close-user-link">
                    <a href="#" onClick={this.props.updateIsShowMyProfile}>Close</a>
                </div>
            </div >
        )
    };
}

export default UserProfile;