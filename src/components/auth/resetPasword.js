import React, { Component } from 'react';
import ToastErr from "../toasts/toastErr";
import ToastInfo from "../toasts/toastInfo";
import axios from 'axios';

class ResetPasword extends Component {

    constructor() {
        super();
        this.state = {
            password: '',
            isValidPassword: false,
            isErrorsText: false,
            isInfoText: false,
        };
    }

    setInfoText = (text) => {
        this.setState({ isInfoText: text });
        setTimeout(this.closeInfoText, 6000);
    }

    closeInfoText = () => {
        this.setState({ isInfoText: false });
    }

    setResetPassword = async () => {
        const config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            }
        }
        const token = this.props.location.search.split('?')[1];

        axios.get(`${process.env.REACT_APP_API_URL}/user/resetpassword`, {
            params: { token: token, password: this.state.password }
        }, config)
            .then(res => {
                localStorage.removeItem('token');
                this.setInfoText('Your password has been changed.');
                window.location = `/`;
            })
            .catch(err => {
                this.setState({ isErrorsText: err.response.data });
                setTimeout(this.handleCloseErrors, 6000);
            })
    }

    handleSave = () => {
        if (!this.state.isValidPassword) {
            this.setState({ isErrorsText: 'Invalid password' });
        } else {
            this.setResetPassword();
        }
        setTimeout(this.handleCloseErrors, 6000);
    }

    handleChangePassword = (event) => {
        this.validatePassword(event.target.value);
        this.setState({ password: event.target.value });
    }

    validatePassword = (value) => {
        const passwordValid = value.match(/^([a-zA-Z0-9+&@#/%?=~_|!:,.;]{1,})$/i);
        const isValidPassword = passwordValid ? true : false;
        this.setState({ isValidPassword: isValidPassword });
    }

    render() {
        return (
            <div className="container" >
                <div className="row justify-content-md-center">
                    <span className="todo-header"><h1>TODOS</h1></span>
                </div>
                <div className="col-md-12">
                    <div className="container_auth">
                        <h1>Reset password</h1>
                        <form action="#" className="sign-in-form">
                            <input
                                type="text"
                                placeholder="new password"
                                className="field"
                                onChange={this.handleChangePassword}
                            />
                            <button
                                type="button"
                                value="Save"
                                onClick={this.handleSave}
                                className="loggedbtn">
                                Save
                        </button>
                        </form>
                        <div className="pass-link">
                            <a href="#" onClick={this.handleSave}>Sign in</a>
                        </div>
                    </div>
                </div>
                {this.state.isErrorsText &&
                    <ToastErr
                        message={this.state.isErrorsText}
                    />
                }
                {this.state.isInfoText &&
                    <ToastInfo
                        message={this.state.isInfoText}
                    />
                }
            </div>
        )
    }
}

export default ResetPasword;