import React, { Component } from 'react';

class TodoRegistration extends Component {

    render() {
        return (
            <div className="col-md-12">
                <div className="container_auth">
                    <h1>Sign up</h1>
                    <form action="#" className="sign-in-form">
                        <input
                            type="text"
                            placeholder="email"
                            className="field"
                            onChange={this.props.handleChangeEmail}
                        />
                        <input
                            type="text"
                            placeholder="lastname"
                            className="field"
                            onChange={this.props.handleChangeLastname}
                        />
                        <input
                            type="text"
                            placeholder="firstname"
                            className="field"
                            onChange={this.props.handleChangeFirstname}
                        />
                        <input
                            type="password"
                            placeholder="password"
                            className="field"
                            onChange={this.props.handleChangePasswod}
                        />
                        <button
                            type="button"
                            value="Sign in"
                            onClick={this.props.handleSignUp}
                            className="loggedbtn">
                            Sign up
                        </button>
                    </form>
                    <div className="pass-link">
                        <a href="#" onClick={this.props.updateIsRegister}>Sign in</a>
                    </div>
                </div>
            </div>
        )
    }
}

export default TodoRegistration;