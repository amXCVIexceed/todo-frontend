import React from 'react';
import { Upload, message } from 'antd';
import ToastErr from "../toasts/toastErr";

class UserAvatar extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      path: false,
      isErrorsText: false,
    }

    this.uploadProps.onChange = this.uploadProps.onChange.bind(this);
  }

  componentDidMount = async () => {
    if (this.props.avatar) {
      this.setState({ path: this.props.avatar });
    }
    else {
      this.setState({ path: false });
    }
  }

  uploadProps = {
    name: 'file',
    action: `${process.env.REACT_APP_API_URL}/user/uploadavatar`,
    headers: {
      authorization: `${localStorage.getItem('token')}`,
    },

    onChange(info) {
      if (info.file.status !== 'uploading') {
      }
      if (info.file.status === 'done') {
        const avatar = info.file.response;
        this.setState({ path: avatar });
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };

  render() {
    return (
      <div className="row avatar justify-content-center">
        {this.state.path ?
          <Upload {...this.uploadProps} showUploadList={false}>
            <a href="#">
              <img src={this.state.path} className="avatar_user_img" />
            </a>
          </Upload>
          :
          <Upload {...this.uploadProps} showUploadList={false}>
            < button type="button" className="sign_btn sign_btn_out" >Download avatar</button>
          </Upload>

        }
        {this.state.isErrorsText &&
          <ToastErr
            message={this.state.isErrorsText}
          />
        }
      </div>
    )
  }
}

export default UserAvatar;
