import React, { Component } from 'react';
import TodoAuth from "./auth/auth.js";
import TodoHeader from "./createToDo/header.js";
import UserProfile from "./auth/userProfile";
import ToastInfo from "./toasts/toastInfo";
import axios from 'axios';

class Main extends Component {

    constructor() {
        super();
        this.state = {
            isLogged: false,
            isValidToken: false,
            isShowMyProfile: false,
            isInfoText: false,
            user: false,
            taskInfo: {
                quantity: null,
                quantityDone: null,
            }
        };
    }

    updateIsLogged = () => {
        this.setState({ isLogged: !this.state.isLogged });
    }

    updateIsShowMyProfile = () => {
        this.setState({ isShowMyProfile: !this.state.isShowMyProfile });
    }

    setInfoText = (text) => {
        this.setState({ isInfoText: text });
        setTimeout(this.closeInfoText, 6000);
    }

    closeInfoText = () => {
        this.setState({ isInfoText: false });
    }

    setTaskInfo = (quantity, quantityDone) => {
        const taskInfo = {
            quantity: quantity,
            quantityDone: quantityDone
        }
        this.setState({ taskInfo: taskInfo });
    }

    getValidityToken = async () => {
        const config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            }
        }
        const data = new URLSearchParams();
        data.append('token', localStorage.getItem('token'));

        axios.get(`${process.env.REACT_APP_API_URL}/user/validity`, { params: { token: localStorage.getItem('token') } }, config)
            .then(res => {
                if (!this.state.isValidToken) {
                    const user = res.data;
                    this.setState({ user: user });
                }
                this.state.isValidToken || this.setState({ isValidToken: true });
            })
            .catch(err => {
                !this.state.isValidToken || this.setState({ isValidToken: false });
            })
    }

    componentDidUpdate = () => {
        this.getValidityToken();
    }

    componentDidMount = () => {
        this.getValidityToken();
    }

    render = () => {
        return (
            <div className="container" >
                <div className="row justify-content-md-center">
                    <span className="todo-header"><h1>TODOS</h1></span>
                </div>

                {this.state.isValidToken ?
                    <TodoHeader
                        updateIsLogged={this.updateIsLogged}
                        updateIsShowMyProfile={this.updateIsShowMyProfile}
                        setTaskInfo={this.setTaskInfo}
                    /> : <TodoAuth
                        updateIsLogged={this.updateIsLogged}
                    />
                }
                {this.state.isValidToken && !this.state.isShowMyProfile ?
                    <div className="my_profile"><a href="#" onClick={this.updateIsShowMyProfile}>My profile</a></div> :
                    <div />
                }
                {this.state.isShowMyProfile ?
                    <UserProfile
                        updateIsShowMyProfile={this.updateIsShowMyProfile}
                        setInfoText={this.setInfoText}
                        user={this.state.user}
                        taskInfo={this.state.taskInfo}
                    /> : <div />
                }
                {this.state.isInfoText ?
                    <ToastInfo
                        message={this.state.isInfoText}
                    /> : <div />}
            </div>
        )
    }
}

export default Main;