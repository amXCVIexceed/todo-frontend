import { Toast, ToastBody, ToastHeader } from 'reactstrap';
import React, { Component } from 'react';

class ToastInfo extends Component {

    render() {
        return (
            <div className='toastr'>
            <Toast isOpen={this.props.toastrIsOpen}>
                <ToastHeader icon="info">
                    Info
                </ToastHeader>
                <ToastBody>
                    {this.props.message}
                </ToastBody>
            </Toast>
        </div>
        )
    };
}

export default ToastInfo;
