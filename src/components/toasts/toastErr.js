import { Toast, ToastBody, ToastHeader } from 'reactstrap';
import React, { Component } from 'react';

class Toast_err extends Component {

    render() {
        return (
            <div className='toastr'>
            <Toast isOpen={this.props.toastrIsOpen}>
                <ToastHeader icon="danger">
                    Error
                </ToastHeader>
                <ToastBody>
                    {this.props.message}
                </ToastBody>
            </Toast>
        </div>
        )
    };
}

export default Toast_err;
