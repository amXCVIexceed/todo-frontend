import React, { Component } from 'react';
import { Route, BrowserRouter as Router } from 'react-router-dom'
import './components/index.css';
import main from './components/main.js';
import ResetPasword from './components/auth/resetPasword'
import axios from 'axios';

class App extends Component {

  componentDidMount = async () => {
    const token = localStorage.getItem('token');
    if (token) {
      axios.defaults.headers.common['Authorization'] = token;
    } else {
      axios.defaults.headers.common['Authorization'] = 'undef';
    }
  }

  render = () => {
    return (
      <Router>
        <div>
          <Route exact path="/" component={main} />
          <Route path="/resetpassword" component={ResetPasword} />
        </div>
      </Router>
    )
  }
};

export default App;
